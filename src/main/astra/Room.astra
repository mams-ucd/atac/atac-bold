import astra.jena.*;

agent Room extends mams.MAMSAgent {
    module Console console;
    module System system;
    module KnowledgeStore static;
    module KnowledgeStore dynamic;
    module TurtleWriter writer;
    module XMLLiteralParser xml;
    module Debug debug;
    module RDFSchema("http://www.w3.org/1999/02/22-rdf-syntax-ns#") rdf;
    module RDFSchema("http://buildsys.org/ontologies/BrickFrame#") brickf;
    module RDFSchema("https://www.w3.org/2019/wot/td#") thingDescription;
    module RDFSchema("http://buildsys.org/ontologies/Brick#") brick;
    module RDFSchema("http://www.w3.org/ns/sosa/") sosa; 
    module RDFSchema("https://www.w3.org/2019/wot/hypermedia#") hypermedia;
    module RDFSchema("http://xmlns.com/foaf/0.1/") foaf;
    module RDFSchema("http://www.w3.org/ns/ssn/") ssn;
    module RDFSchema("http://qudt.org/schema/qudt#") qudt;

    // module KnowledgeStore dynamic;

    types sim {
        formula url(string, string);
        formula iteration(int);
        formula inspecting(string);
        formula inspected(string);
        formula inspectingOccupancy(string);
        formula inspectedOccupancy(string);
        formula inspectingLuminance(string);
        formula inspectedLuminance(string);
        formula light(string, string);
        formula lightSwitch(string, string);
        formula lightSensor(string, string);
        formula occupancySensor(string, string);
        formula fault(string, string);
        formula gettingKnowledge(string);
        formula transition(string, string);
        formula issuingRequest(string);
        formula putTurtle(string, string);
        formula initialised();
    }


    rule +!main([string url]) {
        MAMSAgent::!init();
        if(system.name() == "Room_SOR42_G_15"){
            console.println("Observing: " + url);
            console.println("Room Agent created for "+system.name());
        }
        +url("room", url);
        static.getKnowledge(url);        
    }

    // initial transition("on", "off"), transition("off", "on"); 

    /**
     * Only for first observation
     */
    rule $static.read(string url) : url("room", url) & ~initialised() {
        foreach(brickf.isLocatedIn(string obj, url)){
            !inspectPoint(obj);
        }

        foreach(brickf.isFedBy(url, string lightUrl)){
            !inspectPoint(lightUrl);
        }

        // create beliefs about light switches

        foreach(rdf.type(string url1, brick.qualifiedName("Luminance_Command"))
                    & thingDescription.hasPropertyAffordance(url1, string affordance)
                    & thingDescription.hasForm(affordance, string form)
                    & hypermedia.hasOperationType(form, thingDescription.qualifiedName("writeProperty"))
                    & hypermedia.hasTarget(form, string target)) {

            +lightSwitch(url1, target);
            // if(system.name() == "Room_SOR42_G_15"){}
                // console.println("------------------------------------------------------> Found Switch: "+ target);
        }

        // create beliefs about light sensors (light thresholds)

        foreach(rdf.type(string url1, brick.qualifiedName("Luminance_Sensor")) & sosa.observes(url1, string target)) {

            +lightSensor(url1, target);
            if(system.name() == "Room_SOR42_G_15"){}
        }

        //creates beliefs about occupancy sensors

        foreach(rdf.type(string url1, brick.qualifiedName("Occupancy_Sensor"))
                & sosa.observes(url1, string target)) {
            +occupancySensor(url1, target);
            if(system.name() == "Room_SOR42_G_15"){
                // console.println("occupancyUrl: "+url1+" has target: "+target);

                // console.println("------------------------------------------------------> Found Occupancy Sensor: "+ target);
            }
        }

        //creates beliefs about light feeds

        foreach(brickf.isFedBy(url, string lightUrl) & ssn.hasProperty(lightUrl, string target)) {
            +light(lightUrl, target);
            if(system.name() == "Room_SOR42_G_15"){}
                // console.println("------------------------------------------------------> Found Light: "+ target);
        }

        //creates beliefs about light faults

        foreach(rdf.type(string url1, brick.qualifiedName("Luminance_Alarm"))
                & sosa.observes(url1, string target)) {
            +fault(url1, target);
            if(system.name() == "Room_SOR42_G_15"){}
                // console.println("------------------------------------------------------> Found Fault: "+ target);
        }
        

        if(system.name() == "Room_SOR42_G_15"){
             debug.dumpBeliefs();
        }
           
        +initialised();
    }

    rule @message(inform, string sender, iteration(int iteration)) {
        // if(system.name() == "Room_SOR42_G_15")
        //     console.println("Message from Building Manager");
        
        !getState();
        !actOnState();
    }

    rule +!getState(){
        !clear();

        foreach(lightSwitch(string url, string target)) {
            !readState(target, boolean success);
            if (success ~= true) -lightSwitch(url, target);
        }

        foreach(occupancySensor(string url, string target)){
            !readState(target, boolean success);
            if (success ~= true) -occupancySensor(url, target);
        }

        foreach(light(string url, string target)) {
            !readState(target, boolean success);
            if (success ~= true) -light(url, target);
        }

        foreach(fault(string url, string target)) {
            !readState(target, boolean success);
            if (success ~= true) -fault(url, target);
        }

        foreach(lightSensor(string url, string target)){
            !readState(target, boolean success);
            if (success ~= true) -lightSensor(url, target);
        }
    }

    /**
     * Attempts to load resource 3 times. If it fails 3 times, then the resoure
     * is identified as a failed resource (it returns success = false). The resource
     * should be removed from the list of available resources.
     */
    rule +!readState(string target, boolean success){
        +gettingKnowledge(target);
        int count = 0;
        success = true;
        while (gettingKnowledge(target) & count < 3) {
            try{
                dynamic.getKnowledge(target);
                wait(~gettingKnowledge(target));
            }recover{
                count++;
                system.sleep(50);
            }
        }
        if (count == 3) {
            console.println("MAJOR ERROR: " + target + " does not exist - REMOVING");
            -gettingKnowledge(target);
            success = false;
        }
    }

    rule $dynamic.read(string url) : gettingKnowledge(url){
        -gettingKnowledge(url);
    }

    rule +!actOnState(){
        /**
         * This code checks if the occupancy sensor states if any room is empty and if the light is still on. 
         * If this is the case it issues a PUT request to turn it off
         */

    	// foreach(light(string lightUrl, string target) & brickf.hasPoint(lightUrl, string property)){
        //     if(system.name() == "Room_SOR42_F_19"){
        //         console.println(lightUrl+ " hasPoint: "+property);
        //     }
        // }

        foreach(occupancySensor(string oUrl, string occupancyUrl) & rdf.value(occupancyUrl, string occValue) & lightSwitch(string lUrl, string lightUrl) & rdf.value(lightUrl+"#it", string lightValue)) {
            if(occValue == "off" & lightValue == "on"){
                console.println("Light is on in "+ system.name() + " even though it is empty - Turning light off");
                !put(lightUrl, writer.write(lightUrl, [triple("<#it>", rdf.qualifiedTurtle("value"), xml.asString("off"))]), "text/turtle", string responseCode, string content);
            }
        }

        // Finds light values that had a luminance level below threshold of 500
        // foreach(lightSwitch(string lightUrl) & rdf.value(lightUrl+"#it", string lightSwitchValue)
        //         & occupancySensor(string occupancyUrl) & rdf.value(occupancyUrl, string occValue) 
        //         & lightSensor(string sensorUrl) & qudt.numericValue(sensorUrl, string lightValue)
        //         & light(string lUrl) & rdf.value(lUrl, string lValue)) {

        //     double liValue = xml.doubleValue(lightValue);
        //     if(system.name() == "Room_SOR42_F_19"){
        //         console.println("lValue: "+lValue);
        //     }
        //     // if(occValue == "on" & lightSwitchValue == "on" & lightValue < 500){
        //     //     console.println("Occupancy Sensor in "+ system.name() + " is "+ lightSwitchValue+", light switch is on and the luminance threshold with a value : "+lightValue+" is below the 500 threshold");
        //     // }            
        // }
    }

    rule +!clear(){
        try{
            dynamic.clear();
        }recover{
            system.sleep(50);
            !clear();
        }
    } 

    rule +!inspectPoint(string url) : ~inspected(url) {
        +inspecting(url);
        // console.println("inspecting point..."+url);

        /*
        * Try recover used to try and handle intermittent failure to read resources.
        */
        try{
            static.getKnowledge(url);
        }
        recover{
            -inspecting(url);
            system.sleep(50);
            !inspectPoint(url);
        }
        wait(~inspecting(url));
    }

    rule $static.read(string url) : inspecting(url) {
        // +inspected(url);
        -inspecting(url);
    }
}