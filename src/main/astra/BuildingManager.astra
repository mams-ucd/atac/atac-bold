import astra.jena.*;

agent BuildingManager {
    module Console console;
    module System system;
    module KnowledgeStore store;
    module URLReader reader;
    module RDFSchema("http://www.w3.org/1999/02/22-rdf-syntax-ns#") rdf;
    module RDFSchema("http://buildsys.org/ontologies/BrickFrame#") brickf;
    module RDFSchema("http://buildsys.org/ontologies/Brick#") brick;


    types sim {
        formula url(string, string);
        formula iteration(int);
        formula exploring(string);
        formula explored(string);
        formula initialised();
    }


    rule +!main([string url]) {
        +url("building", url);
        console.println("\n**************\nBuilding Manager Agent Created\n**************\n");
        console.println("Exploring: "+ url);
        store.getKnowledge(url);
    }

    rule @message(inform, string sender, iteration(int iteration)) {
        list children = system.getChildren();
        if (list_count(children) > 0) {
            // console.println("Forwarding iteration: " + iteration + " to: " + children);
            send(inform, children, iteration(iteration));
        }
    }

    /**
     * Only for first observation
     */
    rule $store.read(string url) : url("building", url) {
        foreach (brickf.hasPart(url, string obj)) {
            console.println("Exploring : " + obj);
            !explored(obj);
        } 

        // Now create the room agents...

        foreach(rdf.type(string roomUrl, brick.qualifiedName("Room"))) {

            string name = reader.lastSegment(roomUrl);
            try {
                system.createAgent(name, "Room");
                system.setMainGoal(name, [roomUrl]);
            } recover {
                console.println("SKIPPING: " + name + " - already exists");
            }
        }  

        console.println("Building Manager has explored the entire building");
        +initialised();             
    }

    /***************************************************************************************
     * EXPLORATION RULES
     ***************************************************************************************/

    rule +!explored(string url) : explored(url) {
        // DO NOTHING: goal achieved
    }

    rule +!explored(string url) {
        +exploring(url);
        store.getKnowledge(url);
        wait(~exploring(url));

        foreach (brickf.hasPart(url, string obj)) {
            !explored(obj);
        }
    }


    rule $store.read(string url) : exploring(url) {
        +explored(url);
        -exploring(url);
    }
}